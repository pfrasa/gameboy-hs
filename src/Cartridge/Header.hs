module Cartridge.Header
  ( Header(..)
  , CartridgeDestination(..)
  , extractHeader
  , parseHeader
  ) where

import Data.Text (Text)
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as BS
import Data.Word
import Data.Binary
import Data.Binary.Get
import Data.Bits
import GHC.Int
import Control.Monad
import Data.Text.Encoding (decodeUtf8')

import Util
import Cartridge.Cartridge

newtype HeaderBinary = HeaderBinary ByteString

extractHeader :: Cartridge -> HeaderBinary
extractHeader (Cartridge binary) = HeaderBinary $ BS.take headerLength $ BS.drop headerStart binary

headerStart :: Int64
headerStart = 0x100

headerEnd :: Int64
headerEnd = 0x14F

headerLength :: Int64
headerLength = headerEnd - headerStart + 1

data Header = Header
  { title :: Text
  , cgbFlag :: Word8
  , newLicenseeCode :: Word16
  , sgbFlag :: Word8
  , cartridgeType :: Word8
  , romSize :: Int
  , ramSize :: Int
  , destination :: CartridgeDestination
  , oldLicenseeCode :: Word8
  , version :: Word8
  , headerChecksum :: Word8
  , globalChecksum :: Word16
  } deriving (Eq, Show)

data CartridgeDestination = Japan | Overseas deriving (Eq, Show)

parseHeader :: HeaderBinary -> Maybe Header
parseHeader (HeaderBinary bs) = third <$> runGetMaybe getBinary bs
  where
    getBinary :: Get Header
    getBinary = do
      replicateM_ (0x134 - 0x100) getWord8
      titleBS <- BS.pack <$> replicateM (0x143 - 0x134) getWord8
      cgbFlag <- getWord8
      newLicenseeCode <- getWord16le
      sgbFlag <- getWord8
      cartridgeType <- getWord8
      romSizeByte <- getWord8
      ramSizeByte <- getWord8
      destinationByte <- getWord8
      oldLicenseeCode <- getWord8
      version <- getWord8
      headerChecksum <- getWord8
      globalChecksum <- getWord16le

      title <- maybeToGet "invalid title" $ (rightToMaybe . decodeUtf8' . BS.toStrict) titleBS
      let romSize = 32 * 1024 * (shift 1 (fromIntegral romSizeByte) :: Int)
      ramSize <- maybeToGet "invalid RAM size" $ parseRamSize ramSizeByte
      destination <- maybeToGet "invalid destination" $ parseDestination destinationByte

      return Header { title = title
                    , cgbFlag = cgbFlag
                    , newLicenseeCode = newLicenseeCode
                    , sgbFlag = sgbFlag
                    , cartridgeType = cartridgeType
                    , romSize = romSize
                    , ramSize = ramSize
                    , destination = destination
                    , oldLicenseeCode = oldLicenseeCode
                    , version = version
                    , headerChecksum = headerChecksum
                    , globalChecksum = globalChecksum
                    }

    parseRamSize :: Word8 -> Maybe Int
    parseRamSize 0 = Just 0
    parseRamSize 2 = Just $ 8 * 1024
    parseRamSize 3 = Just $ 32 * 1024
    parseRamSize 4 = Just $ 128 * 1024
    parseRamSize 5 = Just $ 64 * 1024
    parseRamSize _ = Nothing

    parseDestination :: Word8 -> Maybe CartridgeDestination
    parseDestination 0 = Just Japan
    parseDestination 1 = Just Overseas
    parseDestination _ = Nothing