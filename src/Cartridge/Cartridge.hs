module Cartridge.Cartridge
  ( Cartridge(..)
  ) where

import Data.ByteString.Lazy (ByteString)

newtype Cartridge = Cartridge ByteString