module Cartridge
  ( Cartridge(..)
  , Header
  , CartridgeDestination(..)
  , loadHeader
  ) where

import Cartridge.Cartridge
import Cartridge.Header

loadHeader :: Cartridge -> Maybe Header
loadHeader = parseHeader . extractHeader