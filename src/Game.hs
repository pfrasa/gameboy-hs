module Game
  ( Game
  , loadGame
  , loadAddress
  , printGame
  ) where

import qualified Data.Text as T
import Data.IntMap (IntMap, (!?))
import qualified Data.IntMap as IM
import Data.ByteString.Lazy (ByteString)
import Instructions
import Disassembler

newtype Game = Game (IntMap InstructionCall) deriving Show

loadGame :: Instructions -> ByteString -> Maybe Game
loadGame instructions = go IM.empty 0
  where
    go :: IntMap InstructionCall -> Address -> ByteString -> Maybe Game
    go imap _ "" = Just (Game imap)
    go imap currentAddress bs = do
      (nextAddress, [next], remainder) <- disassemble instructions currentAddress 1 bs
      go (IM.insert (getAddress next) next imap) nextAddress remainder

    getAddress :: InstructionCall -> Int
    getAddress (InstructionCall address _ _) = address

loadAddress :: Int -> Game -> Maybe InstructionCall
loadAddress n (Game imap) = imap !? n

printGame :: Game -> String
printGame (Game imap) = T.unpack $ T.intercalate "\n" (map display (IM.elems imap))