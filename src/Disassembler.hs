module Disassembler
  ( InstructionCall(..)
  , Address
  , Argument(..)
  , display
  , disassemble
  , disassembleFrom
  ) where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Word
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as BS
import Data.Binary.Get
import qualified Data.Map as M
import Data.List (intercalate)
import Data.Maybe
import Text.Printf

import Util
import Instructions

type Address = Int
data Argument = Fixed | Value Word16 deriving (Show, Eq)
data InstructionCall = InstructionCall Address Instruction [Argument] deriving (Show, Eq)

display :: InstructionCall -> Text
display (InstructionCall address instruction arguments) =
  let instructionString = printf "%04X %-8s" address instruction.mnemonic
      argumentString = if null arguments then
                          ""
                       else
                          " " <> intercalate ", " (zipWith displayArg instruction.operands arguments)
  in  T.pack $ instructionString <> argumentString
  where
    displayArg :: Operand -> Argument -> String
    displayArg operand argument =
      let argString = case argument of
                        Fixed -> T.unpack operand.name
                        Value val -> printf "0x%x" val
      in  if operand.immediate then argString else "(" <> argString <> ")"

-- return the parsed calls as well as the next address and the remainder of the bytestring
disassemble :: Instructions -> Address -> Int -> ByteString -> Maybe (Address, [InstructionCall], ByteString)
disassemble instructions currentAddress numCalls bs = go currentAddress numCalls [] bs
  where
    go :: Address -> Int -> [InstructionCall] -> ByteString -> Maybe (Address, [InstructionCall], ByteString)
    go address 0 acc bs = Just (address, reverse acc, bs)
    go address numCalls acc bs = do
      (remainder, readBytes, instruction) <- runGetMaybe (getCall address) bs
      go (address + fromIntegral readBytes) (numCalls - 1) (instruction : acc) remainder

    getCall :: Address -> Get InstructionCall
    getCall address = do
      firstByte <- getWord8
      (imap, instructionByte) <- if firstByte == 0xCB then do
        let imap = instructions.cbprefixed
        instructionByte <- getWord8
        return (imap, instructionByte)
      else do
        let imap = instructions.unprefixed
        return (imap, firstByte)

      instruction <- maybeToGet "bad instruction" $ M.lookup (Opcode instructionByte) imap
      arguments <- parseOperands instruction.operands

      return $ InstructionCall address instruction arguments

    parseOperands :: [Operand] -> Get [Argument]
    parseOperands (op:ops) = do
      arg <- parseOperand op
      args <- parseOperands ops
      return (arg:args)
    parseOperands [] = return []

    parseOperand :: Operand -> Get Argument
    parseOperand operand =
      let byteCount = fromMaybe 0 operand.bytes
      in  case byteCount of
            0 -> return Fixed
            1 -> Value . fromIntegral <$> getWord8
            2 -> Value <$> getWord16le
            _ -> fail "invalid byte length"

disassembleFrom :: Instructions -> Address -> Int -> ByteString -> Maybe (Address, [InstructionCall], ByteString)
disassembleFrom instructions startAddress numCalls bs = disassemble instructions startAddress numCalls (BS.drop (fromIntegral startAddress) bs)