module CPU.Evaluate
  ( EvalError(..)
  , evaluate
  ) where

import Data.Text (Text)
import Control.Exception hiding (evaluate)
import Game
import CPU.Registers
import Instructions
import Disassembler
import Util

data EvalError
  = InvalidAddress Address
  | InvalidInstruction Address Text
  deriving Show

instance Exception EvalError

data Action
  = None

newtype GameState = GameState Registers

evaluate :: Game -> GameState -> IO ()
evaluate game state =
  case evaluateOne game state of
    Left error -> throw error
    Right (nextPC, action) -> do
      newState <- performAction action (setPC nextPC state)
      evaluate game newState

evaluateOne :: Game -> GameState -> Either EvalError (Address, Action)
evaluateOne game (GameState registers) = do
  let pc = fromIntegral $ getRegister PC registers
  InstructionCall _ instruction args <- maybeToRight (InvalidAddress pc) $ loadAddress pc game
  let nextPC = pc + instruction.bytes
  case instruction.mnemonic of
    "NOP" -> Right (nextPC, None)
    other -> Left (InvalidInstruction pc other)

performAction :: Action -> GameState -> IO GameState
performAction None = return

setPC :: Address -> GameState -> GameState
setPC address (GameState registers) = GameState (setRegister PC (fromIntegral address) registers)