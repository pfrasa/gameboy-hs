module CPU.Registers.Internal (RegisterVal, Registers(..)) where

import Data.Word

type RegisterVal = Word16

data Registers = Registers
  { af :: RegisterVal
  , bc :: RegisterVal
  , de :: RegisterVal
  , hl :: RegisterVal
  , sp :: RegisterVal
  , pc :: RegisterVal
  } deriving Show
