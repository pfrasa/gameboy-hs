module CPU.Registers
  ( Registers
  , RegisterVal
  , HalfRegisterVal
  , Register(..)
  , HalfRegister(..)
  , Flag(..)
  , initRegisters
  , setFlag
  , unsetFlag
  , checkFlag
  , getRegister
  , getHalfRegister
  , setRegister
  , setHalfRegister
  )
  where

import Data.Word
import Data.Bits
import CPU.Registers.Internal

type HalfRegisterVal = Word8

-- only fully directly accessible ones, exclude AF
data Register = BC | DE | HL | SP | PC deriving (Eq, Show)
data HalfRegister = A | B | C | D | E | H | L deriving (Eq, Show)

data Flag = Zero | Sub | HalfCarry | Carry deriving (Eq, Show)

initRegisters :: Registers
initRegisters = Registers 0 0 0 0 0 0

setFlag :: Flag -> Registers -> Registers
setFlag flag = modifyAf (`setBit` flagBit flag)

unsetFlag :: Flag -> Registers -> Registers
unsetFlag flag = modifyAf (`clearBit` flagBit flag)

checkFlag :: Flag -> Registers -> Bool
checkFlag flag registers = registers.af `testBit` flagBit flag

getRegister :: Register -> Registers -> RegisterVal
getRegister name registers =
  case name of
    BC -> registers.bc
    DE -> registers.de
    HL -> registers.hl
    SP -> registers.sp
    PC -> registers.pc

getHalfRegister :: HalfRegister -> Registers -> HalfRegisterVal
getHalfRegister name registers =
  case name of
    A -> getHighBits registers.af
    B -> getHighBits registers.bc
    C -> getLowBits registers.bc
    D -> getHighBits registers.de
    E -> getLowBits registers.de
    H -> getHighBits registers.hl
    L -> getLowBits registers.hl

setRegister :: Register -> RegisterVal -> Registers -> Registers
setRegister name value registers =
  case name of
    BC -> registers { bc = value }
    DE -> registers { de = value }
    HL -> registers { hl = value }
    SP -> registers { sp = value }
    PC -> registers { pc = value }

setHalfRegister :: HalfRegister -> HalfRegisterVal -> Registers -> Registers
setHalfRegister name value registers =
  case name of
    A -> setHighBits af (\val r -> r { af = val })
    B -> setHighBits bc (\val r -> r { bc = val })
    C -> setLowBits bc (\val r -> r { bc = val })
    D -> setHighBits de (\val r -> r { de = val })
    E -> setLowBits de (\val r -> r { de = val })
    H -> setHighBits hl (\val r -> r { hl = val })
    L -> setLowBits hl (\val r -> r { hl = val })
  where
    setHighBits :: (Registers -> RegisterVal) -> (RegisterVal -> Registers -> Registers) -> Registers
    setHighBits getReg setReg =
      let lowBits = getLowBits (getReg registers)
          newRegisterVal = fromIntegral value `shiftL` 8 + fromIntegral lowBits
      in  setReg newRegisterVal registers

    setLowBits :: (Registers -> RegisterVal) -> (RegisterVal -> Registers -> Registers) -> Registers
    setLowBits getReg setReg =
      let highBits = getHighBits (getReg registers)
          newRegisterVal = fromIntegral highBits `shiftL` 8 + fromIntegral value
      in  setReg newRegisterVal registers

flagBit :: Flag -> Int
flagBit Zero = 7
flagBit Sub = 6
flagBit HalfCarry = 5
flagBit Carry = 4

modifyAf :: (RegisterVal -> RegisterVal) -> Registers -> Registers
modifyAf f registers =
  let newAf = f registers.af
  in  registers { af = newAf }

getHighBits :: RegisterVal -> HalfRegisterVal
getHighBits register = fromIntegral (register `shiftR` 8)

getLowBits :: RegisterVal -> HalfRegisterVal
getLowBits register = fromIntegral (0b11111111 .&. register)