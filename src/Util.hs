module Util
  ( runGetMaybe
  , rightToMaybe
  , maybeToRight
  , maybeToGet
  , third
  ) where

import Data.Binary
import Data.Binary.Get
import Data.ByteString.Lazy (ByteString)

runGetMaybe :: Get a -> ByteString -> Maybe (ByteString, ByteOffset, a)
runGetMaybe getter bs = rightToMaybe $ runGetOrFail getter bs

rightToMaybe :: Either a b -> Maybe b
rightToMaybe = either (const Nothing) Just

maybeToRight :: b -> Maybe a -> Either b a
maybeToRight _ (Just x) = Right x
maybeToRight y Nothing  = Left y

maybeToGet :: String -> Maybe a -> Get a
maybeToGet failString Nothing = fail failString
maybeToGet _ (Just x) = return x

third :: (a, b, c) -> c
third (_, _, x) = x