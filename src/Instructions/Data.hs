{-# LANGUAGE TemplateHaskell #-}

module Instructions.Data (instructionData) where

import Data.ByteString (ByteString)
import Data.FileEmbed

instructionData :: ByteString
instructionData = $(embedFile "data/Opcodes.json")

