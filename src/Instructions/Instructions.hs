module Instructions.Instructions
  ( Instructions(..)
  , Instruction(..)
  , Opcode(..)
  , Operand(..)
  ) where

import Data.Word
import Data.Text (Text)
import Data.Map (Map)

data Instructions = Instructions
  { unprefixed :: Map Opcode Instruction
  , cbprefixed :: Map Opcode Instruction
  } deriving (Show, Eq)

data Instruction = Instruction
  { opcode :: Opcode
  , immediate :: Bool
  , operands :: [Operand]
  , cycles :: [Int]
  , bytes :: Int
  , mnemonic :: Text
  } deriving (Show, Eq)

newtype Opcode = Opcode Word8 deriving (Eq, Ord, Show)

data Operand = Operand
  { immediate :: Bool
  , name :: Text
  , bytes :: Maybe Int
  } deriving (Show, Eq)