{-# LANGUAGE DeriveGeneric #-}

module Instructions.Load (load) where

import GHC.Generics
import Data.ByteString (ByteString)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Map (Map)
import qualified Data.Map as Map
import Instructions.Instructions
import Data.Aeson
import Data.Word
import Numeric

load :: ByteString -> Maybe Instructions
load jsonString = do
  parsed <- decodeStrict jsonString :: Maybe InstructionsJSON
  return $ Instructions{unprefixed=mergeInstructions parsed.unprefixed, cbprefixed=mergeInstructions parsed.cbprefixed}
  where
    mergeInstructions :: Map Text InstructionJSON -> Map Opcode Instruction
    mergeInstructions = Map.mapKeys toOpcode . Map.mapWithKey (\key val ->
      Instruction
        { opcode = toOpcode key
        , immediate = val.immediate
        , operands = (\(OperandJSON operand) -> operand) <$> val.operands
        , cycles = val.cycles
        , bytes = val.bytes
        , mnemonic = val.mnemonic
        })
    
    toOpcode :: Text -> Opcode
    toOpcode = Opcode . readHexByte
      where
        readHexByte :: Text -> Word8
        readHexByte = fst . head . readHex . T.unpack . T.drop 2

newtype OperandJSON = OperandJSON Operand deriving Show

instance FromJSON OperandJSON where
  parseJSON = fmap OperandJSON . withObject "Operand" (\v -> Operand <$> v .: "immediate" <*> v .: "name" <*> v .:? "bytes")

data InstructionJSON = InstructionJSON
  { immediate :: Bool
  , operands :: [OperandJSON]
  , cycles :: [Int]
  , bytes :: Int
  , mnemonic :: Text
  } deriving (Show, Generic)

instance FromJSON InstructionJSON

data InstructionsJSON = InstructionsJSON
  { unprefixed :: Map Text InstructionJSON
  , cbprefixed :: Map Text InstructionJSON
  } deriving (Show, Generic)

instance FromJSON InstructionsJSON