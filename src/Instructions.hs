module Instructions
  ( Instructions(..)
  , Instruction(..)
  , Opcode(..)
  , Operand(..)
  , gameboyInstructions
  ) where

import Instructions.Instructions
import Instructions.Load
import Instructions.Data
import Data.Maybe

gameboyInstructions :: Instructions
gameboyInstructions = fromJust $ load instructionData