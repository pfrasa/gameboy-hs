module CartridgeSpec where

import Test.Hspec
import qualified Data.ByteString.Lazy as BS
import Cartridge
import Cartridge.Header (Header(..))

spec :: Spec
spec = do
  context "loadHeader" $ do
    it "loads snake header correctly" $ do
      cartridge <- Cartridge <$> BS.readFile "data/snake.gb"
      let loaded = loadHeader cartridge
      let expected = Header { title = "Yvar's GB Snake"
                            , cgbFlag = 128
                            , newLicenseeCode = 0
                            , sgbFlag = 0
                            , cartridgeType = 0
                            , romSize = 32 * 1024
                            , ramSize = 0
                            , destination = Overseas
                            , oldLicenseeCode = 0
                            , version = 45
                            , headerChecksum = 66
                            , globalChecksum = 51166
                            }
      
      loaded `shouldBe` Just expected

    it "loads other game header correctly" $ do
      cartridge <- Cartridge <$> BS.readFile "data/W95_b06.gb"
      let loaded = loadHeader cartridge

      title <$> loaded `shouldBe` Just "LINUX GAME PROJ"