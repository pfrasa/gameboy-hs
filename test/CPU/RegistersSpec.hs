{-# LANGUAGE StandaloneDeriving, DeriveGeneric, ScopedTypeVariables #-}

module CPU.RegistersSpec where

import Test.QuickCheck
import Test.Hspec
import Test.Hspec.QuickCheck
import Generic.Random 
import GHC.Generics
import CPU.Registers
import CPU.Registers.Internal

spec :: Spec
spec = do
  flagSpecs Zero
  flagSpecs Sub
  flagSpecs HalfCarry
  flagSpecs Carry

  registerSpecs BC
  registerSpecs DE
  registerSpecs HL
  registerSpecs SP
  registerSpecs PC

  halfRegisterSpecs A
  halfRegisterSpecs B
  halfRegisterSpecs C
  halfRegisterSpecs D
  halfRegisterSpecs E
  halfRegisterSpecs H
  halfRegisterSpecs L
  
  where
    flagSpecs :: Flag -> SpecWith ()
    flagSpecs flag = context (show flag <> " flag") $ do
      it "is unset initially" $ do
        checkFlag flag initRegisters `shouldBe` False

      it "can be set on initial register state" $ do
        let registers = setFlag flag initRegisters
        checkFlag flag registers `shouldBe` True

      it "can be set and unset on initial register state" $ do
        let registers = unsetFlag flag $ setFlag flag initRegisters
        checkFlag flag registers `shouldBe` False

      prop "works properly on arbitrary register state" $ \(registers :: Registers) ->
        checkFlag flag (setFlag flag registers) &&
          checkFlag flag (setFlag flag (setFlag flag registers)) &&
          checkFlag flag (setFlag flag (unsetFlag flag registers)) &&
          not (checkFlag flag (unsetFlag flag registers)) &&
          not (checkFlag flag (unsetFlag flag (unsetFlag flag registers))) &&
          not (checkFlag flag (unsetFlag flag (setFlag flag registers))) &&
          -- no other state is changed upon setFlag
          (flag == Zero || checkFlag Zero (setFlag flag registers) == checkFlag Zero registers) &&
          (flag == Sub || checkFlag Sub (setFlag flag registers) == checkFlag Sub registers) &&
          (flag == HalfCarry || checkFlag HalfCarry (setFlag flag registers) == checkFlag HalfCarry registers) &&
          (flag == Carry || checkFlag Carry (setFlag flag registers) == checkFlag Carry registers) &&
          getHalfRegister A (setFlag flag registers) == getHalfRegister A registers &&
          getRegister BC (setFlag flag registers) == getRegister BC registers &&
          getRegister DE (setFlag flag registers) == getRegister DE registers &&
          getRegister HL (setFlag flag registers) == getRegister HL registers &&
          getRegister SP (setFlag flag registers) == getRegister SP registers &&
          getRegister PC (setFlag flag registers) == getRegister PC registers &&
          -- no other state is changed upon unsetFlag
          (flag == Zero || checkFlag Zero (unsetFlag flag registers) == checkFlag Zero registers) &&
          (flag == Sub || checkFlag Sub (unsetFlag flag registers) == checkFlag Sub registers) &&
          (flag == HalfCarry || checkFlag HalfCarry (unsetFlag flag registers) == checkFlag HalfCarry registers) &&
          (flag == Carry || checkFlag Carry (unsetFlag flag registers) == checkFlag Carry registers) &&
          getHalfRegister A (unsetFlag flag registers) == getHalfRegister A registers &&
          getRegister BC (unsetFlag flag registers) == getRegister BC registers &&
          getRegister DE (unsetFlag flag registers) == getRegister DE registers &&
          getRegister HL (unsetFlag flag registers) == getRegister HL registers &&
          getRegister SP (unsetFlag flag registers) == getRegister SP registers &&
          getRegister PC (unsetFlag flag registers) == getRegister PC registers

    registerSpecs :: Register -> SpecWith ()
    registerSpecs register = context (show register <> " register") $ do
      it "is 0 initially" $ do
        getRegister register initRegisters `shouldBe` 0

      prop "can be set on arbitrary register state" $ \(registers :: Registers, value :: RegisterVal) ->
        let withValueSet = setRegister register value registers
        in  getRegister register withValueSet == value &&
              -- no other state was touched
              (register == BC || getRegister BC withValueSet == getRegister BC registers) &&
              (register == DE || getRegister DE withValueSet == getRegister DE registers) &&
              (register == HL || getRegister HL withValueSet == getRegister HL registers) &&
              (register == SP || getRegister SP withValueSet == getRegister SP registers) &&
              (register == PC || getRegister PC withValueSet == getRegister PC registers) &&
              getHalfRegister A withValueSet == getHalfRegister A registers &&
              checkFlag Zero withValueSet == checkFlag Zero registers &&
              checkFlag Sub withValueSet == checkFlag Sub registers &&
              checkFlag HalfCarry withValueSet == checkFlag HalfCarry registers &&
              checkFlag Carry withValueSet == checkFlag Carry registers

    halfRegisterSpecs :: HalfRegister -> SpecWith ()
    halfRegisterSpecs halfRegister = context (show halfRegister <> " half-register") $ do
      it "is 0 initially" $ do
        getHalfRegister halfRegister initRegisters `shouldBe` 0
      
      prop "can be set on arbitrary register state" $ \(registers :: Registers, value :: HalfRegisterVal) ->
        let withValueSet = setHalfRegister halfRegister value registers
        in  getHalfRegister halfRegister withValueSet == value &&
              -- no other state was touched
              (halfRegister == A || getHalfRegister A withValueSet == getHalfRegister A registers) &&
              (halfRegister == B || getHalfRegister B withValueSet == getHalfRegister B registers) &&
              (halfRegister == C || getHalfRegister C withValueSet == getHalfRegister C registers) &&
              (halfRegister == D || getHalfRegister D withValueSet == getHalfRegister D registers) &&
              (halfRegister == E || getHalfRegister E withValueSet == getHalfRegister E registers) &&
              (halfRegister == H || getHalfRegister H withValueSet == getHalfRegister H registers) &&
              (halfRegister == L || getHalfRegister L withValueSet == getHalfRegister L registers) &&
              getRegister SP withValueSet == getRegister SP registers &&
              getRegister PC withValueSet == getRegister PC registers &&
              checkFlag Zero withValueSet == checkFlag Zero registers &&
              checkFlag Sub withValueSet == checkFlag Sub registers &&
              checkFlag HalfCarry withValueSet == checkFlag HalfCarry registers &&
              checkFlag Carry withValueSet == checkFlag Carry registers

deriving instance Generic Registers

instance Arbitrary Registers where
  arbitrary = genericArbitrary uniform
  shrink = genericShrink