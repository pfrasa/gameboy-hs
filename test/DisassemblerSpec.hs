module DisassemblerSpec where

import Test.Hspec
import qualified Data.ByteString.Lazy as BS
import Data.Maybe
import Instructions
import Disassembler

spec :: Spec
spec = do
  context "disassemble" $ do
    it "disassembles no-ops correctly" $ do
      let (nextAddr, result, remainder) = fromJust $ disassembleFrom gameboyInstructions 0 2 "\NUL\NUL"
      let call byte = InstructionCall byte (Instruction { opcode = Opcode 0
                                                        , immediate = True
                                                        , operands = []
                                                        , cycles = [4]
                                                        , bytes = 1
                                                        , mnemonic = "NOP"
                                                        }) []
      let expected = [call 0, call 1]

      result `shouldBe` expected
      (display . head) result `shouldBe` "0000 NOP     "
      remainder `shouldBe` ""
      nextAddr `shouldBe` 2

    it "disassembles meaningful operation correctly" $ do
      snakeData <- BS.readFile "data/snake.gb"
      let (nextAddr, result, remainder) = fromJust $ disassembleFrom gameboyInstructions 0x201 1 snakeData
      let expected = InstructionCall 0x201 (Instruction { opcode = Opcode 224
                                                        , immediate = False
                                                        , operands = [ Operand { immediate = False, name = "a8", bytes = Just 1 }
                                                                     , Operand { immediate = True, name = "A", bytes = Nothing }
                                                                     ]
                                                        , cycles = [12]
                                                        , bytes = 2
                                                        , mnemonic = "LDH"
                                                        }) [ Value 139
                                                           , Fixed
                                                           ]
      result `shouldBe` [expected]
      (display . head) result `shouldBe` "0201 LDH      (0x8b), A"
      BS.length remainder `shouldBe` BS.length snakeData - 0x203
      nextAddr `shouldBe` 0x203

    it "disassembles sequence of meaningful operations correctly" $ do
      snakeData <- BS.readFile "data/snake.gb"
      let (nextAddr, result, remainder) = fromJust $ disassembleFrom gameboyInstructions 0x150 16 snakeData
      map display result `shouldBe` [ "0150 NOP     "
                                    , "0151 DI      "
                                    , "0152 LD       SP, 0xfffe"
                                    , "0155 LD       B, 0x80"
                                    , "0157 LD       C, 0x0"
                                    , "0159 LDH      A, (0x44)"
                                    , "015B CP       0x90"
                                    , "015D JR       NZ, 0xfa"
                                    , "015F DEC      C"
                                    , "0160 LD       A, C"
                                    , "0161 LDH      (0x42), A"
                                    , "0163 DEC      B"
                                    , "0164 JR       NZ, 0xf3"
                                    , "0166 XOR      A"
                                    , "0167 LDH      (0x40), A"
                                    , "0169 LD       A, 0x0"
                                    ]
      BS.length remainder `shouldBe` BS.length snakeData - 0x16B
      nextAddr `shouldBe` 0x16B